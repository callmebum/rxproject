package call.me.bum.sekeletonproject.models;

public interface RepositoryObserver {

  void onDataChanged(Data data);

}
