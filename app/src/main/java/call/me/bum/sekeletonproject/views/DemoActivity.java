package call.me.bum.sekeletonproject.views;

import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import call.me.bum.sekeletonproject.R;
import call.me.bum.sekeletonproject.adapters.RecyclerViewAdapter;
import call.me.bum.sekeletonproject.models.Data;
import call.me.bum.sekeletonproject.models.RepositoryObserver;
import call.me.bum.sekeletonproject.models.Subject;
import java.util.ArrayList;

public class DemoActivity extends AppCompatActivity implements RepositoryObserver {

  Subject mSubject;

  RecyclerViewAdapter mAdapter;
  final Handler mHandler = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_demo);
    mSubject = Data.getInstance();
    mSubject.regiterObserver(this);
    initRecyclerView();
  }

  private void initRecyclerView() {
    RecyclerView recyclerView = findViewById(R.id.recycler_view);
    mAdapter = new RecyclerViewAdapter(this);
    recyclerView.setAdapter(mAdapter);
    recyclerView.setLayoutManager(new LinearLayoutManager(this));
  }

  @Override
  public void onDataChanged(Data data) {
    ArrayList<Data> datas = new ArrayList<>();
    datas.add(data);
    mAdapter.setDatas(datas);
  }
}
