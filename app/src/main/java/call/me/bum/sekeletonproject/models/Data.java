package call.me.bum.sekeletonproject.models;

import android.os.Handler;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class Data implements Subject {

  private String mTitle;
  private String mImageUrl;

  private List<RepositoryObserver> mObservers;

  private static Data Instance;
  private Data(){
    mObservers = new ArrayList<>();
    addNewData();
  }

  public static Subject getInstance() {
    if (Instance == null)
      Instance = new Data();
    return Instance;
  }

  private void addNewData() {
    this.mTitle = "Lang nghe";;
    this.mImageUrl = "something";
    final Handler handler = new Handler();
    handler.postDelayed(() -> notifyObserver(), 10000);
  }

  public String getTitle() {
    return mTitle;
  }

  public void setTitle(String title) {
    mTitle = title;
  }

  public String getImageUrl() {
    return mImageUrl;
  }

  public void setImageUrl(String imageUrl) {
    mImageUrl = imageUrl;
  }

  @Override
  public void regiterObserver(RepositoryObserver repositoryObserver) {
    if(!mObservers.contains(repositoryObserver)) mObservers.add(repositoryObserver);
  }

  @Override
  public void removeObserver(RepositoryObserver repositoryObserver) {
    if(mObservers.contains(repositoryObserver)) mObservers.remove(repositoryObserver);
  }

  @Override
  public void notifyObserver() {
    for (RepositoryObserver observer : mObservers)
      observer.onDataChanged(this);
  }
}
