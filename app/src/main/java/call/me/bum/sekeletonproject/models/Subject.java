package call.me.bum.sekeletonproject.models;

public interface Subject {

  void regiterObserver(RepositoryObserver repositoryObserver);
  void removeObserver(RepositoryObserver repositoryObserver);

  void notifyObserver();
}
